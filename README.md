Essay-on-Donald-Davidson
========================

A short essay I wrote for a class in 2000. I find the writing really bad when I read it now. I was pretty proud of it when I wrote it ten years ago. I had spent multiple late nightes writing it. Maybe I will spend some time to edit it.

The essay is divided roughly into four sections. The first is on his first theory of meaning from “Meaning and Truth.” The second is based around “Belief and the Basis of Meaning” which is his theory of interpretation. The third and the most lengthy is his theory communication based on “A Nice Derangement of Epitaphs”. The last is a discussion section to talk about his theory and it's impact.

